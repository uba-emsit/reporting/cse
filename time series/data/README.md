The data file presented in this folder contains all CSE time series, listed in their "natural" order.
This can be used to get a quick overview on the database's depth and granularity. The file also contains some time series properties
and metadata, it does not, however, contain the time series values.

### How to update

1. Create list of time series ID in correct order
    1. Create Analyst report in the database
    2. Use the following array formulas:
        1. Cell A1: `=TSIDArrayFromTSFilter("REGION=")` to load IDs of all time series
        2. Cell B1: `=TSIDArraySort(A1:A200000;"CATEGORY:userdefined:asc";"ACTIVITY:userdefined:asc";"TYPE:userdefined:asc";"REGION:userdefined:asc";"SUBSTANCE:userdefined:asc")` to apply correct sorting
    3. Make sure the number of IDs matches the number of time series present in the database
    4. Save the resulting column B as a file to the tools folder at [ordered_ids.csv](../tools/ExportTimeSeries/ordered_ids.csv)
    5. Remove all quotes written by Excel from the ID file as those break the sorting
2. Run export time series tool
3. Check the resulting [data file](timeseries.csv), all time series not sorted correctly will be at the top
4. Remove the Analyst report from the database
