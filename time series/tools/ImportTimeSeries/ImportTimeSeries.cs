﻿using CsvHelper;
using CsvHelper.Configuration;
using Mesap.Framework;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using System.Globalization;
using System.Text;
using UBA.Mesap;

namespace UBA.TimeSeriesManager;

internal class ImportTimeSeries
{
    private const int MaxTimeseriesIdLength = 250;
    private const int MaxTimeseriesNameLength = 250;

    private static readonly CsvConfiguration config = new(CultureInfo.InvariantCulture)
    {
        HasHeaderRecord = true,
        Delimiter = ";",
        Quote = '"'
    };
    private static readonly List<CsvTimeseries> allCsvTimeseries = ReadTimeseriesMapping("../../../../../data/timeseries.csv");

    private static readonly Dictionary<string, string> acceptedNameChanges = [];

    private static int CurrentPosition = 1; // Starts at line 1 as the CSV file has a header
    private static bool UpdateWithoutConfirmation = false;
    private static int valuesChanged = 0;

    static void Main(string[] args)
    {
        ScriptHelper.ConnectAndExecute([StartImport], "ZSE TimeSeries Manager");
    }

    private static void StartImport(Database database)
    {
        Console.WriteLine("Hit enter to start time series checks, use any other key to abort!");
        if (Console.ReadKey().Key == ConsoleKey.Enter)
        {
            List<Timeseries> allMesapTimeseries = new TimeseriesReader(database.DatabaseUserContext).Read(new TimeseriesFilterSettings());
            foreach (CsvTimeseries csvTimeseries in allCsvTimeseries)
            {
                    if (++CurrentPosition % 1000 == 0)
                    Console.WriteLine($"Done with CSV file up to line {CurrentPosition}/{allCsvTimeseries.Count}");

                Timeseries? mesapSeries = allMesapTimeseries.FirstOrDefault(series => series?.PrimaryKey == csvTimeseries.TsNr, null);
                if (mesapSeries != null)
                {
                    CheckTimeseries(database.DatabaseUserContext, mesapSeries, csvTimeseries);
                    allMesapTimeseries.Remove(mesapSeries);
                }
            }
        }
    }

    private static void CheckTimeseries(DatabaseUserContext context, Timeseries mesapSeries, CsvTimeseries csvTimeseries)
    {
        bool idChanged = !mesapSeries.Id.ToString().Equals(csvTimeseries.ID);
        bool nameChanged = !mesapSeries.Name.ToString().Equals(csvTimeseries.Name);
        
        if (idChanged || nameChanged)
        {
            Console.WriteLine($"\nFound time series (ID: {mesapSeries.Id}, at line {CurrentPosition} in CSV file) to be out of sync:");
            if (idChanged)
            {
                Console.WriteLine($"CSV-FILE ID  : '{csvTimeseries.ID}'");
                Console.WriteLine($"Database ID  : '{mesapSeries.Id}'");
            }
            if (nameChanged)
            {
                Console.WriteLine($"CSV-FILE Name: '{csvTimeseries.Name}'");
                Console.WriteLine($"Database Name: '{mesapSeries.Name}'");
            }
            if (UpdateWithoutConfirmation)
                UpdateTimeseries(context, mesapSeries, csvTimeseries);
            else if (!idChanged && acceptedNameChanges.TryGetValue(mesapSeries.Name, out string? value) && value.Equals(csvTimeseries.Name))
            {
                Console.WriteLine($"Applying previously accepted change: {mesapSeries.Name} -> {value}");
                UpdateTimeseries(context, mesapSeries, csvTimeseries);
            }
            else
            {
                Console.WriteLine("Hit <ENTER> to update the time series in the database, <ESC> to finish the importer, <A> to update this and all following timeseries or use any other key to skip!");
                
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Escape:
                        throw new Exception("Get me out of here!");
                    case ConsoleKey.A:
                        UpdateWithoutConfirmation = true;
                        UpdateTimeseries(context, mesapSeries, csvTimeseries);
                        break;
                    case ConsoleKey.Enter:
                        if (nameChanged && !idChanged && mesapSeries.Name.Trim().Length > 0 && csvTimeseries.Name != null)
                            acceptedNameChanges.Add(mesapSeries.Name, csvTimeseries.Name);
                        UpdateTimeseries(context, mesapSeries, csvTimeseries);
                        break;
                }
            }
        }
    }

    private static void UpdateTimeseries(DatabaseUserContext context, Timeseries mesapSeries, CsvTimeseries csvTimeseries)
    {
        mesapSeries.Id = csvTimeseries.ID;
        mesapSeries.Name = csvTimeseries.Name;

        if (new TimeseriesWriter(context).Write(mesapSeries).UpdatedEntities.Count == 1)
        {
            Console.WriteLine($"Change Nr. {++valuesChanged} - Time series (TsNr: {mesapSeries.PrimaryKey}) has now:");
            Console.WriteLine($"Database ID  : '{mesapSeries.Id}'");
            Console.WriteLine($"Database Name: '{mesapSeries.Name}'");
        }
        else
            Console.WriteLine($"FAILED to update time series (TsNr: {mesapSeries.PrimaryKey})!");
    }

    private static List<CsvTimeseries> ReadTimeseriesMapping(string fromFile)
    {
        using (var reader = new StreamReader(fromFile, Encoding.GetEncoding(1252)))
        using (var csvReader = new CsvReader(reader, config))
        {
            List<CsvTimeseries> result = [];

            foreach (CsvTimeseries series in csvReader.GetRecords<CsvTimeseries>())
            {
                if (series.ID == null || series.ID.Length > MaxTimeseriesIdLength)
                    Console.WriteLine($"Bad csv entry skipped: ID is \"{series.ID}\"");
                else if (series.Name == null || series.Name.Length > MaxTimeseriesNameLength)
                    Console.WriteLine($"Bad csv entry skipped: Name is \"{series.Name}\"");
                else if (series.ID != null && series.Name != null)
                    result.Add(series);
                else
                    Console.WriteLine($"Bad csv entry skipped: TsNr is \"{series.TsNr}\"");
            }

            return result;
        }
    }       

    internal class CsvTimeseries
    {
        public int TsNr { get; set; }
        
        private string? _id;
        public string? ID
        {
            get { return _id; }
            set { _id = value?.Trim(); /*_id = ReplaceUmlaute(value?.Trim().Replace(" ", "_")); */ }
        }

        private string? _name;
        public string? Name {
            get { return _name; }
            set { _name = value?.Trim(); }
        }

        private static string? ReplaceUmlaute(string? s)
        {
            return s?.Replace("ä", "ae").Replace("ö", "oe").Replace("ü", "ue").Replace("Ä", "AE").Replace("Ö", "OE").Replace("Ü", "UE").Replace("ß", "ss");
        }
    }
}
