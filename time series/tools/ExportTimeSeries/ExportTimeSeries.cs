﻿using CsvHelper;
using CsvHelper.Configuration;
using Mesap.Framework;
using Mesap.Framework.Collections;
using Mesap.Framework.Common;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using Mesap.Framework.Expressions;
using System.Globalization;
using System.Text;
using UBA.Mesap;

namespace UBA.TimeSeriesManager;

internal class ExportTimeSeries
{
    private const string OrderedIdsFile = "../../../ordered_ids.csv";
    private const string OutputFile = "../../../../../data/timeseries.csv";

    private static readonly CsvConfiguration config = new(CultureInfo.InvariantCulture)
    {
        HasHeaderRecord = true,
        Delimiter = ";",
        Quote = '"',
        // ShouldQuote = (args) => true
    };

    static void Main(string[] args)
    {
        ScriptHelper.ConnectAndExecute([StartExport], appName: "ZSE TimeSeries Manager");
    }

    private static void StartExport(Database database)
    {
        Console.WriteLine("Hit enter to start time series export, use any other key to abort!");
        if (Console.ReadKey().Key == ConsoleKey.Enter)
        {
            // Prepare list of documentation items to read for each time series, speeds things up...
            AnnexComponent uncertainty = database.AnnexComponents["UNCERTAINTY"];
            AnnexItem distribution = database.AnnexItems[new AnnexItemKey(uncertainty, "DISTRIBUTION")];
            AnnexItem umax = database.AnnexItems[new AnnexItemKey(uncertainty, "Umax")];
            AnnexItem umin = database.AnnexItems[new AnnexItemKey(uncertainty, "Umin")];

            // Read all time series and their relevant documentation
            TimeseriesReader reader = new(database.DatabaseUserContext);
            List<Timeseries> list = reader.Read().OrderBy(series => series.PrimaryKey).ToList();
            List<AnnexObjectTimeseriesDocu> docuList = new AnnexObjectReader(database.DatabaseUserContext).ReadDocu(list, TimeResolution.Year);
            new DocuReader(database.DatabaseUserContext).Read(docuList, [distribution, umax, umin]);

            Console.WriteLine($"Found {list.Count} time series with {docuList.Count} documentation sets in database, starting sort operation...");
            if (File.Exists(OrderedIdsFile)) 
            {
                Console.WriteLine("Pre-defined time series order found, applying that...");
                string[] lines = File.ReadAllLines(OrderedIdsFile, Encoding.GetEncoding(1252));
                list = list.OrderBy(series => Array.IndexOf(lines, series.Id.ToString())).ToList();
            }
            else
                Console.WriteLine("No pre-defined time series order found, using default...");

            Console.WriteLine($"Done sorting time series, collecting detailed infos now...");
            List<CsvTimeseries> records = [];
            for (int index = 0; index < list.Count; index++)
            {
                AnnexObjectTimeseriesDocu? documentation = docuList.Find(item => item.Timeseries.Equals(list[index]));
                if (documentation != null)
                    docuList.Remove(documentation); // Reduce search space for the next round...

                records.Add(new CsvTimeseries(list[index], documentation));
                if ((index + 1) % 100 == 0)
                    Console.WriteLine($"Collected details for {index + 1} of {list.Count} time series...");
            }
            
            Console.WriteLine($"Writing time series details to file...");
            using (var writer = new StreamWriter(OutputFile, append: false, Encoding.GetEncoding(1252)))
            using (var csv = new CsvWriter(writer, config))
            {
                csv.WriteRecords(records);
            }

            Console.WriteLine($"Wrote {records.Count} time series definitions to file.");
        }
    }

    internal class CsvTimeseries(Timeseries series, AnnexObjectTimeseriesDocu? documentation)
    {
        private readonly Timeseries _series = series;
        private readonly List<Descriptor> _descriptors = series.Keys.Select(key => key.Descriptor).ToList();
        private readonly AnnexObjectTimeseriesDocu? _documentation = documentation;

        public int TsNr => _series.PrimaryKey;
        public string ID => _series.Id;
        public string Name => _series.Name;
        public string REGION => GetStringForDimension("REGION");
        public string CATEGORY => GetStringForDimension("CATEGORY");
        public string ACTIVITY => GetStringForDimension("ACTIVITY"); 
        public string TYPE => GetStringForDimension("TYPE");
        public string SUBSTANCE => GetStringForDimension("SUBSTANCE");
        public string FUEL => GetStringForDimension("FUEL");
        public string POWER_HEAT_WORK => GetStringForDimension("POWER_HEAT_WORK");
        public string FIRING_TECH => GetStringForDimension("FIRING_TECH");
        public string ENERGY_BALANCE_FLAG => GetStringForDimension("EB_FLAG");
        public string ENERGY_BALANCE_ROW => GetStringForDimension("EB_ROW");
        public string PERMIT => GetStringForDimension("PERMIT");
        public string TRANSPORT_TRINITY => GetStringForDimension("TRANSPORT_TRINITY");
        public string VEHICLE_TYPE => GetStringForDimension("VEHICLE_TYPE");
        public string EURO_NORM => GetStringForDimension("EURO_NORM");
        public string PRODUCT => GetStringForDimension("PRODUCT");
        public string SPECIES => GetStringForDimension("SPECIES");
        public string MANURE_MANAGEMENT => GetStringForDimension("MANURE_MANAGEMENT");
        public string LUC_FROM => GetStringForDimension("LUC_FROM");
        public string LUC_TO => GetStringForDimension("LUC_TO");
        public string POOL => GetStringForDimension("POOL");
        public string Unit => _series.CalcUnit;
        public bool IsVirtual => _series.VirtualType == TimeseriesVirtualType.Virtual;
        public string VirtualFormula => _series.VirtualFormula.Replace("\n", "").Replace("\r", "");
        public string Interpolation => _series.Properties[TimeResolution.Year].InterpolationRule.ToString();
        public string Extrapolation => _series.Properties[TimeResolution.Year].ExtrapolationRule.ToString();
        public bool IsHistoryEnabled => !_series.Properties[TimeResolution.Year].NoHistory;
        public string? UncertaintyDistribution => GetUncertaintyDocumentation("DISTRIBUTION");
        public string? UncertaintyUmin => GetUncertaintyDocumentation("Umin");
        public string? UncertaintyUmax => GetUncertaintyDocumentation("Umax");

        private string GetStringForDimension(string dimension)
        {
            return string.Join('*', _descriptors.Where(key => key.Dimension.Id == dimension).Select(key => key.Id));
        }

        private string? GetUncertaintyDocumentation(string field)
        {
            if (_documentation != null && _documentation.AnnexValueComponents.Any(c => c.Component.Id == "UNCERTAINTY"))
                return field.Equals("DISTRIBUTION") ?
                    _documentation.GetAnnexValue<AnnexValueTextPool>("UNCERTAINTY", field).Value.TextData :
                    _documentation.GetAnnexValue<AnnexValueNumber>("UNCERTAINTY", field).Value.ToString();
            else
                return null;
        }
    }
}
