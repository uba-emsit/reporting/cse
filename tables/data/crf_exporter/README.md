### Steps to prepare CRF reporter import

* Export all variables from the Mesap CRF exporter to xml files
    * Be sure to enable confidentiality checks where needed (CRF2 and CRF5)!
    * Also save the exporter logs files for reference to issues/changes/problems
    * To streamline the process, only export variables known to have been changed since the last export (CAUTION: this only works if ZSE database data is not changed!)
* Export full variable list and update file [crf variables](<crf variables.csv>)
    * Use MS Access export option and copy resulting table to Excel
    * Rearrange columns to match existing file [crf variables](<crf variables.csv>)
    * Replace boolean values (0 => False, -1 => True)
    * Sort variables (descriptor, name, attribute, gas, uid)
    * Fix Excel issue that changes 2Ex and 4Ex to numbers in scientific notation
    * Understand and check changes
    * Update sorted party profile link columns
* Create [replace_zeros](<crf variables.csv>) file from full variable list in [crf variables](<crf variables.csv>)
    * Remove unneeded columns and sort by uid
    * Combine columns "notation_key", "list" and "text" into one column using Excel
* Run Python scripts from the import file base folder in order:
    1. [sort_var_by_uid.py](reporter_import_files/sort_var_by_uid.py) to make sure xml files get comparable
    2. [replace_zeros.py](reporter_import_files/replace_zeros.py) to use add notation keys and other info
    3. [replace_c_in_emissions.py](reporter_import_files/replace_c_in_emissions.py) to allow emission calculation in CRF reporter to function
    4. [combine_in_one_file.py](reporter_import_files/combine_in_one_file.py) to create the single "import.xml" file
* Manually patch import file as needed for variables:
    * 0113FB69-E411-405F-B1D5-5B11B0A54A36
    * 63F06F7D-7A42-4601-A5B6-59C07E85FBB4
    * 6A3306C9-B9A5-46A7-B589-17F02D1683E2
    * 8E3D06CD-79CC-4883-904D-185306574D1B
    * BF73B220-D44F-41AB-B562-A51EBA4A0E08
    * 1DAAD6F6-C5D7-4C35-A4D9-79C8447E6622
    * 38F7B7FA-0074-427D-8CF0-22FA066408FB
    * 9E1E3394-B982-48AC-A37D-FC42BA6D9B40
    * EA927C4F-1BD3-4229-824A-F5F5C0C8099F
    * F2C07836-113B-4AAA-A57E-E5B8713E730F
    * 11BEA716-F666-452F-8D89-2B4F3C024ACF
    * ADA8496F-19BE-4DDA-93E0-9484E2981114
    * BCD7A57F-2630-4A9D-9721-BE55D0232FBA
    * DAAE0D8D-F68E-4EFD-B8EE-9B882F5B57B0
    * 71598479-5880-40E7-8A2D-322BC2930B7F
    * A899688A-19F5-4C19-9281-36E9EF067AAB
* Carefully understand the differences between the previous import files version and the newly created file
* Import to [CRF Reporter](https://crfappar5.unfccc.int/crfapp)
* Download import feedback report
* Download CRF tables 
