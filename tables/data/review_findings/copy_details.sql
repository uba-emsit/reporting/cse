UPDATE Ev_Review_Findings_Air
SET    DETAILS = [ZSE_Submission_2023].[dbo].[Event_13].[Value_141]
FROM   Ev_Review_Findings_Air
    JOIN [ZSE_Submission_2023].[dbo].[Event_13]
        ON OLD_ID = [ZSE_Submission_2023].[dbo].[Event_13].[EventNr]
        