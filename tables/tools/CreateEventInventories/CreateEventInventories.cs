﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using UBA.Mesap;

namespace UBA.Events;

internal class CreateEventInventories
{
    static void Main(string[] args)
    {
        ScriptHelper.ConnectAndExecute([Create], "ZSE Event Manager");
    }

    private static void Create(Database db)
    {
        EventItemTypeSpecificationDate yearOnlySpec = new() { UserFormat = "YYYY", DateFormat = global::Mesap.Framework.Common.DateFormat.UserDefined };
        EventItemTypeSpecificationTextPool listSpec = new() { MultiSelect = true, AllowedAdd = true, UseId = false };
        EventItemTypeSpecificationMemo filterSpec = new() { IsSetting = true };
        //DefaultEventValueString refValue = new("REF");
        DefaultEventValueBool exportDefaultValue = new(false);
        DefaultEventValueBool filterManualDefaultValue = new(false);

        if (!db.EventInventories.Any(inv => inv.Id == "crf_variables"))
        {
            EventInventory crf_vars = CreateEventInventory(db, id: "crf_variables", name: "CRF-Variablen",
                dimension: db.Dimensions.First(dim => dim.Id == "CATEGORY"), navColumnName: "CategoryNr");
            CreateEventInventoryField(db, crf_vars, id: "uid", name: "UID", columnName: "UID", EventItemType.String, null, mandatory: true);
            CreateEventInventoryField(db, crf_vars, id: "name", name: "Name", columnName: "Name", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "commodity", name: "Commodity", columnName: "Commodity", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "source", name: "Source", columnName: "Source", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "target", name: "Target", columnName: "Target", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "option", name: "Option", columnName: "Option", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "method", name: "Method", columnName: "Method", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "activity", name: "Activity", columnName: "Activity", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "attribute", name: "Attribute", columnName: "Attribute", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "gas", name: "Gas", columnName: "Gas", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "unit", name: "Einheit", columnName: "Unit", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "notation_key", name: "Notation Key", columnName: "NotationKey", EventItemType.TextPool);
            CreateEventInventoryField(db, crf_vars, id: "list", name: "Auswahl", columnName: "Selection", EventItemType.TextPool, listSpec);
            CreateEventInventoryField(db, crf_vars, id: "text", name: "Textinfo (englisch)", columnName: "Information", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "specify_activity_data", name: "Aktivitätsdaten (englisch)", columnName: "ActivityData", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "comment_ts", name: "Zeitreihenkommentar (englisch)", columnName: "CommentTimeSeries", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "comment_cell_nk_explanation", name: "NE/IE explanation (englisch)", columnName: "NotationKeyInfo", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "tsid", name: "Zeitreihen-IDs", columnName: "TimeSeriesID", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "filter_settings", name: "Filter", columnName: "Filter", EventItemType.Memo, filterSpec);
            CreateEventInventoryField(db, crf_vars, id: "no_ts_filter", name: "Anzahl Zeitreihen", columnName: "TimeSeriesCount", EventItemType.String);
            CreateEventInventoryField(db, crf_vars, id: "hypothesis", name: "Hypothese", columnName: "Hypothesis", EventItemType.TextPool, null, mandatory: true);
            CreateEventInventoryField(db, crf_vars, id: "filter_manual", name: "Filter manuell?", columnName: "FilterManual", EventItemType.Boolean, null, mandatory: true, filterManualDefaultValue);
            CreateEventInventoryField(db, crf_vars, id: "export", name: "Export?", columnName: "Export", EventItemType.Boolean, null, mandatory: true, exportDefaultValue);
            CreateEventInventoryField(db, crf_vars, id: "internal_comment", name: "Interner Kommentar (deutsch, bleibt im Exporter)", columnName: "InternalComment", EventItemType.String);

            Console.WriteLine($"Created table '{crf_vars.Name}'");
        }

        if (!db.EventInventories.Any(inv => inv.Id == "crf_export"))
        {
            EventInventory crf_export = CreateEventInventory(db, id: "crf_export", name: "CRF-Export");
            CreateEventInventoryField(db, crf_export, id: "uid", name: "UID", columnName: "UID", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "name", name: "Name", columnName: "Name", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "commodity", name: "Commodity", columnName: "Commodity", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "source", name: "Source", columnName: "Source", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "target", name: "Target", columnName: "Target", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "option", name: "Option", columnName: "Option", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "method", name: "Method", columnName: "Method", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "activity", name: "Activity", columnName: "Activity", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "attribute", name: "Attribute", columnName: "Attribute", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "gas", name: "Gas", columnName: "Gas", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "unit", name: "Einheit", columnName: "Unit", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "year", name: "Jahr", columnName: "Year", EventItemType.Date, yearOnlySpec);
            CreateEventInventoryField(db, crf_export, id: "year-uid", name: "Jahres-UID", columnName: "YearUID", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "value", name: "Wert", columnName: "Value", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "specify_activity_data", name: "Aktivitätsdaten", columnName: "ActivityData", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "comment_ts", name: "Zeitreihenkommentar", columnName: "CommentTimeSeries", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "comment_cell_nk_explanation", name: "NE/IE explanation", columnName: "NotationKeyInfo", EventItemType.String);
            CreateEventInventoryField(db, crf_export, id: "user", name: "Nutzer", columnName: "User", EventItemType.User);

            Console.WriteLine($"Created table '{crf_export.Name}'");
        }

        if (!db.EventInventories.Any(inv => inv.Id == "mapping_descriptors"))
        {
            EventInventory crf_descriptors = CreateEventInventory(db, id: "mapping_descriptors", name: "Mapping CRF Deskriptoren");
            CreateEventInventoryField(db, crf_descriptors, id: "crf_dimension", name: "CRF-Dimension", columnName: "CRFDimension", EventItemType.String, null, mandatory: true);
            CreateEventInventoryField(db, crf_descriptors, id: "crf_descriptor", name: "CRF-Deskriptor", columnName: "CRFDescriptor", EventItemType.String, null, mandatory: true);
            CreateEventInventoryField(db, crf_descriptors, id: "mesap_dimension", name: "Mesap-Dimension", columnName: "MesapDimension", EventItemType.String, null, mandatory: true);
            CreateEventInventoryField(db, crf_descriptors, id: "tree", name: "Mesap-Baum", columnName: "MesapTree", EventItemType.String);
            CreateEventInventoryField(db, crf_descriptors, id: "tree-node_descriptor", name: "Mesap-Baumknoten oder Deskriptor", columnName: "MesapTreeObject", EventItemType.String, null, mandatory: true);

            Console.WriteLine($"Created table '{crf_descriptors.Name}'");
        }

        if (!db.EventInventories.Any(inv => inv.Id == "mapping_units"))
        {
            EventInventory crf_units = CreateEventInventory(db, id: "mapping_units", name: "Mapping CRF Einheiten");
            CreateEventInventoryField(db, crf_units, id: "crf_unit", name: "Einheit im CRF-Reporter", columnName: "CRFUnit", EventItemType.String);
            CreateEventInventoryField(db, crf_units, id: "mesap_unit", name: "Einheit in Mesap", columnName: "MesapUnit", EventItemType.String, null, mandatory: true);

            Console.WriteLine($"Created table '{crf_units.Name}'");
        }

        if (!db.EventInventories.Any(inv => inv.Id == "mapping_years"))
        {
            EventInventory crf_years = CreateEventInventory(db, id: "mapping_years", name: "Mapping CRF Jahre");
            CreateEventInventoryField(db, crf_years, id: "year_uid", name: "UID im CRF-Reporter", columnName: "UID", EventItemType.String, null, mandatory: true);
            CreateEventInventoryField(db, crf_years, id: "year", name: "Jahr", columnName: "Year", EventItemType.Date, yearOnlySpec, mandatory: true);

            Console.WriteLine($"Created table '{crf_years.Name}'");
        }

        EventItemTypeSpecificationNumber numberSpec = new() { DecimalPlaces = 2, NumberFormat = global::Mesap.Framework.Common.NumberFormat.Fix };
        EventItemTypeSpecificationDescriptor pollutantSpec = new() { DimensionPrimaryKey = db.Dimensions["SUBSTANCE"].PrimaryKey, UseId = true };
        EventItemTypeSpecificationCalculated trendSpec = new() { NumberFormat = global::Mesap.Framework.Common.NumberFormat.Fix, DecimalPlaces = 2, Formula = "(1 - {EM_LY} / {EM_BY}) * -100" };
        DefaultEventValueBool confidentialDefaultValue = new(false);

        if (!db.EventInventories.Any(inv => inv.Id == "KCA_UNCERTAINTY"))
        {
            EventInventory uncert_kca = CreateEventInventory(db, id: "KCA_UNCERTAINTY", name: "Hauptquellgruppen und Unsicherheiten", tableName: "Ev_KCA_Uncertainty",
                dimension: db.Dimensions.First(dim => dim.Id == "CATEGORY"), navColumnName: "CATEGORY");
            CreateEventInventoryField(db, uncert_kca, id: "QUALIFIER", name: "Zusatz", columnName: "QUALIFIER", EventItemType.String, null, mandatory: false, description: "Zusätzliche Qualifikation des Datensatzes, zum Beispiel \"all fuels\" oder \"glass\"");
            CreateEventInventoryField(db, uncert_kca, id: "SUBSTANCE", name: "Schadstoff", columnName: "SUBSTANCE", EventItemType.Descriptor, pollutantSpec, mandatory: true);
            CreateEventInventoryField(db, uncert_kca, id: "BASE_YEAR", name: "Basisjahr", columnName: "BASE_YEAR", EventItemType.Date, null, mandatory: true, description: "Frühestes Jahr in der Trendbetrachtung");
            CreateEventInventoryField(db, uncert_kca, id: "LATEST_YEAR", name: "Aktueller Rand", columnName: "LATEST_YEAR", EventItemType.Date, null, mandatory: true, description: "Spätestes Jahr in der Trendbetrachtung");
            CreateEventInventoryField(db, uncert_kca, id: "SUBMISSION", name: "Berichtsjahr", columnName: "SUBMISSION", EventItemType.Date, null, mandatory: true, description: "Jahr der Berichterstattung");
            CreateEventInventoryField(db, uncert_kca, id: "TOTAL", name: "Gesamtemission", columnName: "TOTAL", EventItemType.TextPool, null, mandatory: true, description: "Verwendete Gesamtemissionsmenge, z.B. mit/ohne LULUCF oder adjusted/non-adjusted");
            CreateEventInventoryField(db, uncert_kca, id: "CONFIDENTIAL", name: "Vertraulich?", columnName: "CONFIDENTIAL", EventItemType.Boolean, null, mandatory: true, defaultValue: confidentialDefaultValue, description: "Müssen die enthaltenen Emissionswerte bei Veröffentlichung mit \"C\" ersetzt werden?");
            CreateEventInventoryField(db, uncert_kca, id: "TOOL", name: "Werkzeug", columnName: "TOOL", EventItemType.TextPool, null, mandatory: true, description: "Software, die zur Berechnung des Datensatzes verwendet wurde.");
            CreateEventInventoryField(db, uncert_kca, id: "EM_1990", name: "Emissionen 1990 [kt|t|kg]", columnName: "EM_1990", EventItemType.Number, numberSpec, mandatory: false, description: "Einheit des Wertes in Abhängigkeit der Spalte Schadstoff. Alle Treibhausgase und klassischen Luftschadstoffe sind in Kilotonnen [kt], Schwermetalle in Tonnen [t] und POPs in Kilogramm [kg] angegeben.");
            CreateEventInventoryField(db, uncert_kca, id: "EM_BY", name: "Emissionen im Basisjahr [kt|t|kg]", columnName: "EM_BY", EventItemType.Number, numberSpec, mandatory: true, description: "Einheit des Wertes in Abhängigkeit der Spalte Schadstoff. Alle Treibhausgase und klassischen Luftschadstoffe sind in Kilotonnen [kt], Schwermetalle in Tonnen [t] und POPs in Kilogramm [kg] angegeben.");
            CreateEventInventoryField(db, uncert_kca, id: "EM_LY", name: "Emissionen am aktuellen Rand [kt|t|kg]", columnName: "EM_LY", EventItemType.Number, numberSpec, mandatory: true, description: "Einheit des Wertes in Abhängigkeit der Spalte Schadstoff. Alle Treibhausgase und klassischen Luftschadstoffe sind in Kilotonnen [kt], Schwermetalle in Tonnen [t] und POPs in Kilogramm [kg] angegeben.");
            CreateEventInventoryField(db, uncert_kca, id: "EM_T", name: "Trend [%]", columnName: "EM_T", EventItemType.Calculated, trendSpec, mandatory: false, description: "Prozentuale Änderung der Emissionen am aktuellen Rand gegenüber dem Basisjahr.");
            CreateEventInventoryField(db, uncert_kca, id: "KCA_1990", name: "HQA: Kumulierte Anteile 1990 [%]", columnName: "KCA_1990", EventItemType.Number, numberSpec, mandatory: false, description: "Hauptquellgruppe, wenn Wert kleiner oder gleich 95% (THG) bzw. 80% (Luft)");
            CreateEventInventoryField(db, uncert_kca, id: "KCA_BY", name: "HQA: Kumulierte Anteile Basisjahr [%]", columnName: "KCA_BY", EventItemType.Number, numberSpec, mandatory: false, description: "Hauptquellgruppe, wenn Wert kleiner oder gleich 95% (THG) bzw. 80% (Luft)");
            CreateEventInventoryField(db, uncert_kca, id: "KCA_LY", name: "HQA: Kumulierte Anteile Aktueller Rand [%]", columnName: "KCA_LY", EventItemType.Number, numberSpec, mandatory: false, description: "Hauptquellgruppe, wenn Wert kleiner oder gleich 95% (THG) bzw. 80% (Luft)");
            CreateEventInventoryField(db, uncert_kca, id: "KCA_T", name: "HQA: Kumulierte Anteile Trend [%]", columnName: "KCA_T", EventItemType.Number, numberSpec, mandatory: false, description: "Hauptquellgruppe, wenn Wert kleiner oder gleich 95% (THG) bzw. 80% (Luft)");
            CreateEventInventoryField(db, uncert_kca, id: "KCA_LY_A2", name: "HQA: Kumulierte Anteile Aktueller Rand (Ansatz 2) [%]", columnName: "KCA_LY_A2", EventItemType.Number, numberSpec, mandatory: false, description: "Hauptquellgruppe, wenn Wert kleiner oder gleich 90% (nur THG)");
            CreateEventInventoryField(db, uncert_kca, id: "KCA_T_A2", name: "HQA: Kumulierte Anteile Trend (Ansatz 2) [%]", columnName: "KCA_T_A2", EventItemType.Number, numberSpec, mandatory: false, description: "Hauptquellgruppe, wenn Wert kleiner oder gleich 90% (nur THG)");
            CreateEventInventoryField(db, uncert_kca, id: "EP_BY_AD", name: "FF: Basisjahr Aktivitätsratenunsicherheit [%]", columnName: "EP_BY_AD", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_BY_EF", name: "FF: Basisjahr Emissionsfaktorunsicherheit [%]", columnName: "EP_BY_EF", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_BY_EM", name: "FF: Basisjahr Emissionsunsicherheit [%]", columnName: "EP_BY_EM", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_BY_VAR", name: "FF: Basisjahr Beitrag zur Varianz [1]", columnName: "EP_BY_VAR", EventItemType.Number, numberSpec, mandatory: false, description: "Die Berechnung der Varianz erfolgt unter Verwendung der Gesamtemissionsmenge gemäß Spalte \"Total\".");
            CreateEventInventoryField(db, uncert_kca, id: "EP_LY_AD", name: "FF: Aktuelle Aktivitätsratenunsicherheit [%]", columnName: "EP_LY_AD", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_LY_EF", name: "FF: Aktuelle Emissionsfaktorunsicherheit [%]", columnName: "EP_LY_EF", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_LY_EM", name: "FF: Aktuelle Emissionsunsicherheit [%]", columnName: "EP_LY_EM", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_LY_VAR", name: "FF: Aktueller Beitrag zur Varianz [1]", columnName: "EP_LY_VAR", EventItemType.Number, numberSpec, mandatory: false, description: "Die Berechnung der Varianz erfolgt unter Verwendung der Gesamtemissionsmenge gemäß Spalte \"Total\".");
            CreateEventInventoryField(db, uncert_kca, id: "EP_T_SA", name: "FF: Trend Typ A Sensitivität [%]", columnName: "EP_T_SA", EventItemType.Number, numberSpec, mandatory: false, description: "Die Berechnung der Sensitivität erfolgt unter Verwendung der Gesamtemissionsmenge gemäß Spalte \"Total\".");
            CreateEventInventoryField(db, uncert_kca, id: "EP_T_SB", name: "FF: Trend Typ B Sensitivität [%]", columnName: "EP_T_SB", EventItemType.Number, numberSpec, mandatory: false, description: "Die Berechnung der Sensitivität erfolgt unter Verwendung der Gesamtemissionsmenge gemäß Spalte \"Total\".");
            CreateEventInventoryField(db, uncert_kca, id: "EP_T_AD", name: "FF: Trend Beitrag Aktivitätsraten [%]", columnName: "EP_T_AD", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_T_EF", name: "FF: Trend Beitrag Emissionsfaktoren [%]", columnName: "EP_T_EF", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_T_EM", name: "FF: Trend Beitrag zur nationalen Gesamtemissionsmenge [%]", columnName: "EP_T_EM", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_BY_AD_Umin", name: "MC: Basisjahr Aktivitätsratenunsicherheit [-%]", columnName: "MC_BY_AD_Umin", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_BY_AD_Umax", name: "MC: Basisjahr Aktivitätsratenunsicherheit [+%]", columnName: "MC_BY_AD_Umax", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_BY_EF_Umin", name: "MC: Basisjahr Emissionsfaktorunsicherheit [-%]", columnName: "MC_BY_EF_Umin", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_BY_EF_Umax", name: "MC: Basisjahr Emissionsfaktorunsicherheit [+%]", columnName: "MC_BY_EF_Umax", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_BY_EM_Umin", name: "MC: Basisjahr Emissionsunsicherheit [-%]", columnName: "MC_BY_EM_Umin", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_BY_EM_Umax", name: "MC: Basisjahr Emissionsunsicherheit [+%]", columnName: "MC_BY_EM_Umax", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_BY_VAR", name: "MC: Basisjahr Beitrag zur Varianz [1]", columnName: "MC_BY_VAR", EventItemType.Number, numberSpec, mandatory: false, description: "Die Berechnung der Varianz erfolgt unter Verwendung der Gesamtemissionsmenge gemäß Spalte \"Total\".");
            CreateEventInventoryField(db, uncert_kca, id: "MC_LY_AD_Umin", name: "MC: Aktuelle Aktivitätsratenunsicherheit [-%]", columnName: "MC_LY_AD_Umin", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_LY_AD_Umax", name: "MC: Aktuelle Aktivitätsratenunsicherheit [+%]", columnName: "MC_LY_AD_Umax", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_LY_EF_Umin", name: "MC: Aktuelle Emissionsfaktorunsicherheit [-%]", columnName: "MC_LY_EF_Umin", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_LY_EF_Umax", name: "MC: Aktuelle Emissionsfaktorunsicherheit [+%]", columnName: "MC_LY_EF_Umax", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_LY_EM_Umin", name: "MC: Aktuelle Emissionsunsicherheit [-%]", columnName: "MC_LY_EM_Umin", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_LY_EM_Umax", name: "MC: Aktuelle Emissionsunsicherheit [+%]", columnName: "MC_LY_EM_Umax", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_LY_VAR", name: "MC: Aktueller Beitrag zur Varianz [1]", columnName: "MC_LY_VAR", EventItemType.Number, numberSpec, mandatory: false, description: "Die Berechnung der Varianz erfolgt unter Verwendung der Gesamtemissionsmenge gemäß Spalte \"Total\".");
            CreateEventInventoryField(db, uncert_kca, id: "MC_T_EM_Umin", name: "MC: Emissionstrendunsicherheit [-%]", columnName: "MC_T_EM_Umin", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_T_EM_Umax", name: "MC: Emissionstrendunsicherheit [+%]", columnName: "MC_T_EM_Umax", EventItemType.Number, numberSpec, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "CNT_AD", name: "Anzahl Aktivitätsraten [1]", columnName: "CNT_AD", EventItemType.Int32, null, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "CNT_EF", name: "Anzahl Emissionsfaktoren [1]", columnName: "CNT_EF", EventItemType.Int32, null, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "CNT_EM", name: "Anzahl Emissionen [1]", columnName: "CNT_EM", EventItemType.Int32, null, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_CORR_AD", name: "EP: Korrelation Aktivitätsraten aktiviert?", columnName: "EP_CORR_AD", EventItemType.Boolean, null, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "EP_CORR_EF", name: "EP: Korrelation Emissionsfaktoren aktiviert?", columnName: "EP_CORR_EF", EventItemType.Boolean, null, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_DRAWS", name: "MC: Anzahl Ziehungen [1]", columnName: "MC_DRAWS", EventItemType.Int32, null, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "MC_ABORT", name: "MC: Abbruchkriterium", columnName: "MC_ABORT", EventItemType.TextPool, null, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "ERRORS", name: "Fehlermeldungen", columnName: "ERRORS", EventItemType.Memo, null, mandatory: false);
            CreateEventInventoryField(db, uncert_kca, id: "SORT", name: "Sortierung", columnName: "SORT", EventItemType.Int32, null, mandatory: false, description: "Hilfsfeld zur Sortierung von Datensätzen mit identischen Quellgruppen und Schadstoffen");

            Console.WriteLine($"Created table '{uncert_kca.Name}'");
        }

        EventItemTypeSpecificationTextPool categorySpec = new() { MultiSelect = true, AllowedAdd = true, UseId = false };
        EventItemTypeSpecificationUser responsibleSpec = new() { MultiSelect = true, UseId = false };

        if (!db.EventInventories.Any(inv => inv.Id == "REVIEW_FINDINGS_AIR"))
        {
            EventInventory review = CreateEventInventory(db, id: "REVIEW_FINDINGS_AIR", name: "Reviewergebnisse Luftschadstoffe", tableName: "Ev_Review_Findings_Air");
            CreateEventInventoryField(db, review, id: "ASPECT", name: "Aspekt", columnName: "ASPECT", EventItemType.TextPool, null, mandatory: true);
            CreateEventInventoryField(db, review, id: "CATEGORY", name: "Quellgruppe", columnName: "CATEGORY", EventItemType.TextPool, categorySpec, mandatory: false);
            CreateEventInventoryField(db, review, id: "FINDING", name: "Zusammenfassung", columnName: "FINDING", EventItemType.Memo, null, mandatory: true);
            CreateEventInventoryField(db, review, id: "DETAILS", name: "Details", columnName: "DETAILS", EventItemType.Memo, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "CLRTAP_2010", name: "CLRTAP 2010", columnName: "CLRTAP_2010", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "CLRTAP_2014", name: "CLRTAP 2014", columnName: "CLRTAP_2014", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "CLRTAP_2022", name: "CLRTAP 2022", columnName: "CLRTAP_2022", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "CLRTAP_2023", name: "CLRTAP 2023", columnName: "CLRTAP_2023", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "NECD_2017", name: "NECD 2017", columnName: "NECD_2017", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "NECD_2018", name: "NECD 2018", columnName: "NECD_2018", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "NECD_2019", name: "NECD 2019", columnName: "NECD_2019", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "NECD_2020", name: "NECD 2020", columnName: "NECD_2020", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "NECD_2021", name: "NECD 2021", columnName: "NECD_2021", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "NECD_2022", name: "NECD 2022", columnName: "NECD_2022", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "NECD_2023", name: "NECD 2023", columnName: "NECD_2023", EventItemType.String, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "IMPLEMENTED", name: "Umgesetzt?", columnName: "IMPLEMENTED", EventItemType.TextPool, null, mandatory: true);
            CreateEventInventoryField(db, review, id: "COMMENT_IIR", name: "IIR-Kommentar (englisch)", columnName: "COMMENT_IIR", EventItemType.Memo, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "RESPONSIBLE", name: "Ansprechpartner", columnName: "RESPONSIBLE", EventItemType.User, responsibleSpec, mandatory: true);
            CreateEventInventoryField(db, review, id: "COMMENT_INTERNAL", name: "Interner Kommentar", columnName: "COMMENT_INTERNAL", EventItemType.Memo, null, mandatory: false);
            CreateEventInventoryField(db, review, id: "OLD_ID", name: "OldID", columnName: "OLD_ID", EventItemType.Int32, null, mandatory: false);

            Console.WriteLine($"Created table '{review.Name}'");
        }
    }

    private static EventInventory CreateEventInventory(Database db, string id, string name,
        string? tableName = null, Dimension? dimension = null, string? navColumnName = "NavNr")
    {
        EventInventory inventory = dimension is null ? new EventInventory(id) : new EventInventory(id, dimension);
        if (dimension is not null)
            inventory.ColumnNameNavObjNr = navColumnName;
        if (tableName is not null)
            inventory.TableName = tableName;

        // inventory.Type = EventType.Document;
        inventory.Name = name;
        inventory.IsWebVisible = false;
        inventory.IsWriteProtected = false;
        inventory.HasHistory = false;
        inventory.NavObjType = dimension is null ? EventNavObjType.None : EventNavObjType.Descriptor;

        inventory.NamedColumns = tableName is not null;
        inventory.ColumnNameId = "RecordID";
        inventory.ColumnNameEventNr = "RecordNr";
        inventory.ColumnNameChangeID = "ChangeID";
        inventory.ColumnNameChangeDate = "ChangeDate";
       
        new EventInventoryWriter(db.DatabaseUserContext).Write(inventory);
        
        return inventory;
    }

    private static EventItem CreateEventInventoryField(Database db, EventInventory inventory, string id, string name, string columnName,
        EventItemType fieldType, IEventItemTypeSpecification? spec = null, bool mandatory = false, DefaultEventValue? defaultValue = null,
        string? description = null)
    {

        EventItem field = new EventItem(id, inventory, fieldType)
        {
            Name = name,
            ColumnNameValue = columnName,
            IsRequired = mandatory                
        };
        if (spec is not null)
            field.ItemTypeSpecification = spec;
        if (defaultValue is not null)
            field.DefaultValue = defaultValue;
        if (description is not null)
            field.Description = description;
        new EventItemWriter(db.DatabaseUserContext).Write(field);

        return field;
    }
}