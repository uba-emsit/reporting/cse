﻿using CsvHelper;
using CsvHelper.Configuration;
using Mesap.Framework;
using Mesap.Framework.Common;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using System.Globalization;
using System.IO.Compression;
using System.Text;
using UBA.Mesap;

namespace UBA.Events;

internal class MaintainCRFExporter
{
    private static readonly CsvConfiguration config = new CsvConfiguration(CultureInfo.InvariantCulture)
    {
        HasHeaderRecord = true,
        Delimiter = ";",
        Quote = '"'
    };
    private static readonly Dictionary<string, CsvVariable> csv_vars = ReadVariables("../../../all_crt_variables.csv");

    static void Main(string[] args)
    {
        ScriptHelper.ConnectAndExecute([Maintain], "ZSE Event Manager");
    }

    private static void Maintain(Database db)
    {
        EventInventory mesap_vars = db.EventInventories["crf_variables"];

        var reader = new EventReader(db.DatabaseUserContext);
        List<Event> events = reader.Read(mesap_vars);

        Console.WriteLine($"Found inventory of {events.Count} variables.");

        EventFilterFactory eventFilterFactory = mesap_vars.CreateFilterFactory(db.DatabaseUserContext);
        EventWriter writer = new EventWriter(db.DatabaseUserContext);
        var count = 1;
        foreach (KeyValuePair<string, CsvVariable> entry in csv_vars)
        {
            EventFilter eventFilter;
            if (eventFilterFactory.TryCreateFilter($"uid = '{entry.Key}'", out eventFilter))
            {
                if (reader.Read(mesap_vars, eventFilter).FirstOrDefault() is { } variable)
                {
                    variable.TrySetValue("category", entry.Value.Category);
                    variable.TrySetValue("classification", entry.Value.Classification);
                    variable.TrySetValue("measure", entry.Value.Measure);
                    variable.TrySetValue("measure_type", entry.Value.MeasureType);
                    variable.TrySetValue("parameter", entry.Value.Parameter);

                    // Update the changed event(s)                    
                    var writeResult = writer.Write(variable, DataWriterOrigin.Unknown);
                    if (writeResult.FailedEntities.Count > 0)
                        Console.WriteLine($"{count++}/{csv_vars.Count} Failed to update variable with UID '{entry.Key}'");
                    else
                        Console.WriteLine($"{count++}/{csv_vars.Count} Updated variable with UID '{entry.Key}'");
                }
                else
                {
                    Console.WriteLine($"{count++}/{csv_vars.Count} No variable found for UID '{entry.Key}'");
                }
            }
        }
    }

    private static Dictionary<string, CsvVariable> ReadVariables(string fromFile)
    {
        IEnumerable<CsvVariable> all_vars;
        Dictionary<string, CsvVariable> result = new();

        using (var reader = new StreamReader(fromFile, Encoding.GetEncoding(1252)))
        using (var csvReader = new CsvReader(reader, config))
        {
            all_vars = csvReader.GetRecords<CsvVariable>();

            foreach (CsvVariable @var in all_vars)
            {
                result.Add(var.UID, var);
            }
        }

        return result;
    }

    internal class CsvVariable
    {
        public string UID { get; set; }
        public string? Category { get; set; }
        public string? Classification { get; set; }
        public string? Measure { get; set; }
        public string? MeasureType { get; set; }
        public string? Parameter { get; set; }

        public override string? ToString()
        {
            return $"{UID}: {Category}, {Classification}, {Measure}, {MeasureType}, {Parameter}";
        }
    }
}
