Imported file: 5_Validated_20250227-142006.json
Imported From: DEU-CRT-2025-V0.4
Imported to: DEU-CRT-2025-V0.4
Imported on: 2025-02-27 14:20
Imported by: Kevin Hausmann

Loading all records

Saved value for year 1998 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 110.849.
Saved value for year 1999 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 221.697.
Saved value for year 2000 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 338.688.
Saved value for year 2001 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 489.228.
Saved value for year 2002 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 751.41.
Saved value for year 2003 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 833.626.
Saved value for year 2004 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 864.054.
Saved value for year 2005 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1005.5.
Saved value for year 2006 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 884.
Saved value for year 2007 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1036.9.
Saved value for year 2008 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1060.8.
Saved value for year 2009 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1068.9.
Saved value for year 2010 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 981.8.
Saved value for year 2011 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1051.8.
Saved value for year 2012 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1428.2.
Saved value for year 2013 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1136.5.
Saved value for year 2014 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1240.2.
Saved value for year 2015 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1250.5.
Saved value for year 2016 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 1003.3.
Saved value for year 2017 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 949.9.
Saved value for year 2018 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 676.6.
Saved value for year 2019 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 661.
Saved value for year 2020 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 666.8.
Saved value for year 2021 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 608.4.
Saved value for year 2022 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 577.7.
Saved value for year 2023 for variable [5.B.2.b  Other - Template][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 67a20513591bebbf1b9b43d1: 577.7.
Saved value for year 1998 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 110.849.
Saved value for year 1999 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 221.697.
Saved value for year 2000 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 338.688.
Saved value for year 2001 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 489.228.
Saved value for year 2002 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 751.41.
Saved value for year 2003 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 833.626.
Saved value for year 2004 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 864.054.
Saved value for year 2005 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1005.5.
Saved value for year 2006 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 884.
Saved value for year 2007 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1036.9.
Saved value for year 2008 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1060.8.
Saved value for year 2009 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1068.9.
Saved value for year 2010 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 981.8.
Saved value for year 2011 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1051.8.
Saved value for year 2012 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1428.2.
Saved value for year 2013 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1136.5.
Saved value for year 2014 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1240.2.
Saved value for year 2015 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1250.5.
Saved value for year 2016 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 1003.3.
Saved value for year 2017 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 949.9.
Saved value for year 2018 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 676.6.
Saved value for year 2019 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 661.
Saved value for year 2020 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 666.8.
Saved value for year 2021 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 608.4.
Saved value for year 2022 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 577.7.
Saved value for year 2023 for variable [5.B.2.b  Other][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 430ac7a6-08ba-4cd7-827b-849970a3008f: 577.7.
Saved value for year 1998 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 410.551.
Saved value for year 1999 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 821.1.
Saved value for year 2000 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 1254.4.
Saved value for year 2001 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 1811.9560000000001.
Saved value for year 2002 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 2783.
Saved value for year 2003 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 3087.504.
Saved value for year 2004 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 3410.228.
Saved value for year 2005 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 3526.374.
Saved value for year 2006 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 3628.093.
Saved value for year 2007 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 4119.381.
Saved value for year 2008 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 4149.088.
Saved value for year 2009 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 4470.4400000000005.
Saved value for year 2010 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 4398.744.
Saved value for year 2011 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 5369.787.
Saved value for year 2012 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 6094.036.
Saved value for year 2013 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 5681.283.
Saved value for year 2014 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 6751.614.
Saved value for year 2015 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 6686.061.
Saved value for year 2016 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 6638.231.
Saved value for year 2017 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 6635.299999999999.
Saved value for year 2018 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 6253.6.
Saved value for year 2019 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 6413.9.
Saved value for year 2020 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 6236.8.
Saved value for year 2021 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 7110.9.
Saved value for year 2022 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 7577.9929999999995.
Saved value for year 2023 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Annual waste amount treated][no gas][no parameter][kt dm] with uid 7746e06f-eee4-4294-8cd1-6a71bb690c6f: 7993.79.
Saved value for year 1998 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 47.834645634768876.
Saved value for year 1998 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03284997478997737.
Saved value for year 1999 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 47.83475873827792.
Saved value for year 1999 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03285000608939228.
Saved value for year 2000 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 47.83476052295918.
Saved value for year 2000 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03285.
Saved value for year 2001 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 47.83363028682816.
Saved value for year 2001 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.032850002980204816.
Saved value for year 2002 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 47.83475961192957.
Saved value for year 2002 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.032850000000000004.
Saved value for year 2003 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 47.83476892661517.
Saved value for year 2003 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03285000116599039.
Saved value for year 2004 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 39.83819152267825.
Saved value for year 2004 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03359829020229733.
Saved value for year 2005 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 45.627028556812185.
Saved value for year 2005 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03216883121302505.
Saved value for year 2006 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 51.652147119712744.
Saved value for year 2006 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.034035563586710706.
Saved value for year 2007 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 49.03460587889297.
Saved value for year 2007 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03367293532693382.
Saved value for year 2008 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 51.976344536437885.
Saved value for year 2008 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03349482103054937.
Saved value for year 2009 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 54.19899226921734.
Saved value for year 2009 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03424032086327072.
Saved value for year 2010 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 60.84564666641206.
Saved value for year 2010 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03495599652991854.
Saved value for year 2011 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 52.60357414549217.
Saved value for year 2011 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.036185684832564124.
Saved value for year 2012 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 48.43813932178936.
Saved value for year 2012 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03445378727660946.
Saved value for year 2013 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 55.997687916620244.
Saved value for year 2013 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03599807297048923.
Saved value for year 2014 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 50.905126862998976.
Saved value for year 2014 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03673397649806402.
Saved value for year 2015 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 46.723044674584926.
Saved value for year 2015 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.03658361029012448.
Saved value for year 2016 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 42.26948215571287.
Saved value for year 2016 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.0381987158928335.
Saved value for year 2017 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 41.24926288185915.
Saved value for year 2017 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.038557864753666.
Saved value for year 2018 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 42.56861647690929.
Saved value for year 2018 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.040131284380197.
Saved value for year 2019 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 41.09187233976208.
Saved value for year 2019 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.040362416002744046.
Saved value for year 2020 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 41.67306631605951.
Saved value for year 2020 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.04018887891226269.
Saved value for year 2021 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 40.58082521199849.
Saved value for year 2021 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.04114985444880395.
Saved value for year 2022 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 41.04892817926858.
Saved value for year 2022 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.041569474925616856.
Saved value for year 2023 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][CH₄][no parameter][g/kg] with uid 53936878-a13d-4ab9-9770-30d5710d1cd7: 41.0668357562558.
Saved value for year 2023 for variable [5.B.2. Anaerobic digestion at biogas facilities][no classification][Implied emission factor][N₂O][no parameter][g/kg] with uid c2ea2bda-ad76-4d55-a169-5cc1731f8ef7: 0.04174791306751866.
Import finished (166.65 seconds)
Import completed successfully!
