### Steps to prepare CRT import

* Export all variables from the Mesap CRF exporter to xml files
    * Be sure to disable confidentiality checks for all exports!
        * None of the variables should export any data that cannot be part of the CRTs!
    * Also save the exporter logs files for reference to issues/changes/problems
    * To streamline the process, only export variables known to have been changed since the last export (CAUTION: this only works if ZSE database data is not changed!)
* Export xml patch files with variables in need for special treatment
* Create replace_zeros.csv file from full variable list
    * Remove unneeded columns and sort by uid
    * Combine columns "notation_key", "list" and "text" into one column using Excel
* Run Python scripts from the import file base folder in order:
    1. [replace_special_variables.py](tools/xml/replace_special_variables.py) to handle variables that need extra care
    2. [replace_zeros.py](tools/xml/replace_zeros.py) to use add notation keys and other info
    3. [sort_var_by_uid.py](tools/xml/sort_var_by_uid.py) to make sure xml files get comparable
    4. [convert.py](tools/json/convert.py) to create JSON import file
* Carefully understand the differences between the previous import files version and the newly created file
* Import to [ERT Reporter Tools](https://etf-ghg.unfccc.int)
    * Also save the import log files for reference to issues/changes/problems
    * When moving to a new "version" in the ERT tools, importing twice solves some issues with missing emissions
* Compare CRT with CSE values using the Analyst reports until the numbers match 
* Allow experts time to review and manually check the tables
* Download/Submit CRT tables and JSON
