import glob
import json
import xml.etree.ElementTree as ET

from enum import StrEnum, auto

# Start script configuration

START_YEAR = 1990
END_YEAR = 2023 # inclusive

INCLUDE_COMMENTS = False # Enable to include both variable and node comments
INCLUDE_VAR_NAMES = False # Enable to include variable name info for debugging

# End script configuration

NOTATION_KEYS = ['NA', 'NO', 'NE', 'IE', 'NR', 'C']

SPECIAL_VALUE_MAPPING = {
    'EMPTY': 0,     # invalid
    'C' : 1,        # 2**0
    'CR': 2,        # 2**1
    'CS': 4,        # 2**2
    'D': 8,         # 2**3
    'GCV': 16,      # 2**4
    'IE': 32,       # 2**5
    'IO': 64,       # 2**6
    'M': 128,       # 2**7
    'NA': 256,      # 2**8
    'NCV': 512,     # 2**9
    'NE': 1024,     # 2**10
    'NO': 2048,     # 2**11
    # 2**12 is reserved
    'OTH': 8192,    # 2**13
    'PS': 16384,    # 2**14
    'R':  32768,    # 2**15
    'RA': 65536,    # 2**16
    'T1': 131072,   # 2**17
    'T1a': 262144,  # 2**18
    'T1b': 524288,  # 2**19
    'T1c': 1048576, # 2**20
    'T2': 2097152,  # 2**21
    'T3': 4194304,  # 2**22
    'FX': 8388608   # 2**23
}

STRING_DROPDOWN_VALUES = [
    'Ammonia Production'
]

class VarType(StrEnum):
    NUMBER = auto()
    TEXT = auto()
    DROPDOWN = auto()
    NK = 'NK'

    @classmethod
    def from_xml(cls, xml_var_type: str):
        match xml_var_type:
            case 'number': return cls.NUMBER
            case 'text': return cls.TEXT
            case 'list': return cls.DROPDOWN
            # JSON type "NK" (notation key) cannot be mapped as it does not exist in XML

class CommentType(StrEnum):
    OFFICIAL_COMMENT = auto()
    NK_EXPLANATION = auto()
    ALLOCATION_BY_PARTY = auto()
    ALLOCATION_BY_IPCC = auto()

    @classmethod
    def from_xml(cls, xml_comment_type: str):
        match xml_comment_type:
            case 'Comment': return cls.OFFICIAL_COMMENT
            case 'NK Explanation': return cls.NK_EXPLANATION
            case 'Allocation by party': return cls.ALLOCATION_BY_PARTY
            case 'Allocation by IPCC': return cls.ALLOCATION_BY_IPCC

def prepare_json_record(variable: ET.Element, year: int) -> dict:
    """Convert XML variable content to JSON format."""
    # Step 1: Prepare variable type and value for given year
    var_uid = variable.attrib['uid']
    var_type = VarType.from_xml(variable.attrib['type'].lower())
    xml_value = variable.find(f'*/year[@name="{year}"]').find('*/value').text
    
    # Handle the special case of the newly introduced notation key type "NK" not present in XML
    if var_type is VarType.NUMBER and xml_value in NOTATION_KEYS:
        var_type = VarType.NK

    try:
        var_value = prepare_value(var_type, xml_value)
    except:
        # Catch odd cases where the XML value is empty or otherwise undefined
        if xml_value: # Skip boring cases where there is simply no value present
            print(f'Found bad value "{xml_value}" with type "{var_type}" for variable "{var_uid}"')
        var_value = xml_value
    
    # Step 2: Create JSON record and put value data
    json_record = {'variable_uid': var_uid.lower(),
                   'value': {
                       'type': var_type,
                       'value': var_value
                  }}
    
    if INCLUDE_VAR_NAMES:
        json_record['name'] = variable.attrib['name']

    # Step 3: Add overall and year-specific variable comments
    if INCLUDE_COMMENTS:
        json_comments = []
    
        xml_var_comments = variable.findall('*/comment')
        xml_var_comments += (variable.find(f'*/year[@name="{year}"]').findall('*/*/comment'))
        for xml_var_comment in xml_var_comments:
            json_comments.append(prepare_comment(var_type, xml_var_comment))                                                 
        
        if len(json_comments) > 0:
           json_record['comments'] = json_comments 
    
    return json_record

def prepare_value(var_type: VarType, xml_value: str):
    """Helper to convert the actual variable value depending on the var's type."""
    match var_type:
        case VarType.NUMBER: 
            return float(xml_value.replace(',', ''))
        case VarType.NK:
            return SPECIAL_VALUE_MAPPING[xml_value]
        case VarType.DROPDOWN:
            values = xml_value.split(',')
            if all([value in STRING_DROPDOWN_VALUES for value in values]):
                # Use string dropdown values
                return xml_value
            else:
                # Convert key bitmap to sum of 2^x values
                return sum([SPECIAL_VALUE_MAPPING[key.strip()] for key in xml_value.split(',')])
        case _: 
            return xml_value

def prepare_comment(var_type: VarType, xml_comment) -> dict[str, CommentType]:
    """Helper to make sure the comment to be added has the correct type."""
    comment_type = CommentType.from_xml(xml_comment.find('type').text)
    comment_value = xml_comment.find('text').text

    match var_type:
        case VarType.NUMBER:
            comment_type = CommentType.OFFICIAL_COMMENT
        case VarType.NK:
            if comment_type is CommentType.OFFICIAL_COMMENT:
                comment_type = CommentType.NK_EXPLANATION

    return {'comment': comment_value,
            'type': comment_type}

# Visit all *.xml files in current directory tree, convert them
# and save the result under a new name.
for filepath in glob.glob('**/*.xml', recursive=True):
    # Step 0: Preload main value array with inventory year sub-arrays
    json_values = []
    for year in range(START_YEAR, END_YEAR + 1):
        json_values.append({'inventory_year': str(year), 'values': []})

    # Step 1: Visit all variables for each year and convert one value at a time
    for variable in ET.parse(filepath).find('variables'):
        for year in range(START_YEAR, END_YEAR + 1):
            # Skip years where variable has no values
            if variable.find(f'*/year[@name="{year}"]') is not None:
                json_record = prepare_json_record(variable, year)
                json_values[year - START_YEAR]['values'].append(json_record)
    
    # Step 2: Convert node comments
    node_comments = []
    if INCLUDE_COMMENTS:
        for node_comment in ET.parse(filepath).find('nodeComments'):
            content = node_comment.find('comment/text').text
            if content:
                node_comments.append({'id': node_comment.attrib['uid'],
                                    'comments': [{'text': node_comment.find('comment/text').text,
                                                'years': [year for year in range(START_YEAR, END_YEAR + 1)]}]})
    
    # Step 3: Embed in JSON metadata header, make sure this matches your situation
    json_output = {
        'version': {
            'metadata_ver': '1.30.3',
            'data_version': "DEU-CRT-2025-V0.4",
            'country': 'DEU',
            'metadata_type': 'CRT'
        },
        'data': {
            'values': json_values,
            'node_comments': node_comments
        }
    }

    # Step 4: Save file with appended '.json'    
    with open(f'{filepath}.json', 'w') as outfile:
        outfile.write(json.dumps(json_output, indent=4))
