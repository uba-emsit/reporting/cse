import glob
import xml.etree.ElementTree as ET

from tree_helpers import sort_children_by, prettify_tree

TEMPLATE = 'empty.xml'
RESULT = 'import.xml'
PATCH = '.patch.xml'

tree = ET.parse(TEMPLATE)

for filepath in glob.glob('**/*.xml', recursive=True):
    if not filepath.endswith(TEMPLATE) and not filepath.endswith(RESULT) and not filepath.endswith(PATCH):
        vars = ET.parse(filepath).find('variables')
        for var in vars.findall('variable'):
            if len(var.get('uid')) > 3: # Skip vars with bad UIDs
                tree.find('variables').append(var)

sort_children_by(tree.find('variables'), 'uid')
prettify_tree(tree).write(RESULT)
