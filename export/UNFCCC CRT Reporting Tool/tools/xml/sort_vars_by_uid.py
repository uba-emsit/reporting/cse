import glob
import xml.etree.ElementTree as ET

from tree_helpers import sort_children_by, prettify_tree

for filepath in glob.glob('**/*.xml', recursive=True):
    tree = ET.parse(filepath)
    
    sort_children_by(tree.find('variables'), 'uid')

    # To be sure, also sort the individual years, typically from 1990 ASC
    for year_range in tree.findall('variables/variable/years'):
        sort_children_by(year_range, 'name')

    prettify_tree(tree).write(filepath)
