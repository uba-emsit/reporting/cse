import glob
import xml.etree.ElementTree as ET

SPECIAL_VARS_FILENAME_ENDING = '.patch.xml'

replacement_dict = {}
for filepath in glob.glob(f'**/*{SPECIAL_VARS_FILENAME_ENDING}', recursive=True):
    with open(filepath, mode='r') as in_file:
        replacement_dict.update({var.get('uid'):var for var in ET.parse(filepath).findall('.//variable')})

for filepath in glob.glob('**/*.xml', recursive=True):
    if not filepath.endswith(SPECIAL_VARS_FILENAME_ENDING):
        tree = ET.parse(filepath)
        
        for uid, var in replacement_dict.items():
            node_to_replace = tree.find(f".//variable[@uid='{uid}']")
            
            if node_to_replace is not None:
                tree.find('variables').remove(node_to_replace)
                tree.find('variables').append(var)

        tree.write(filepath)
