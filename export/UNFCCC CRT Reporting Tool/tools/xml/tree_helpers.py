import xml.etree.ElementTree as ET

def sort_children_by(parent, attr):
    parent[:] = sorted(parent, key=lambda child: child.get(attr))

def prettify_tree(tree):
    ET.indent(tree, space="  ", level=0)

    xmlstr = ET.tostring(tree.getroot(), encoding='unicode')
    xmlstr = xmlstr.replace('\n          <record>\n            ', '<record>')
    xmlstr = xmlstr.replace('\n          </record>\n        </year>', '</record></year>')
    xmlstr = xmlstr.replace('\n            <comments />', '<comments />')
    return ET.ElementTree(ET.fromstring(xmlstr))
