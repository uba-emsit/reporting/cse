import xml.etree.ElementTree as ET

UID_FILE = 'uids.txt'
DATA_FILE = 'data.xml'
RESULT_FILE = 'result.csv'

START_YEAR = 1990
END_YEAR = 2023 # inclusive

xml_root = ET.parse(DATA_FILE).getroot()

with open(UID_FILE, 'r') as uid_file, open(RESULT_FILE, mode='w') as result_file:
    for uid in uid_file.read().splitlines():
        variable = xml_root.find(f"variables/variable[@uid='{uid}']")

        values = []    
        if variable is not None:
            for year in range(START_YEAR, END_YEAR + 1):
                year_value = variable.find(f"years/year[@name='{year}']/record/value")

                if year_value is not None:
                    values.append(year_value.text)
                else:
                    print(f"Variable with UID {uid} has no value for {year} in file {DATA_FILE}!")
                    values.append('---NO_VALUE---')
        else:
            print(f"Variable with UID {uid} does not exist in file {DATA_FILE}!")
        
        value_string = values.__str__().replace("'", "") \
                                       .replace("[", "") \
                                       .replace("]", "") \
                                       .replace(", ", ";") \
                                       .replace(",", "") \
                                       .replace(".", ",")
        result_file.write(f"{uid};{value_string}\n")
