﻿using Mesap.Framework;

namespace UBA.Mesap;

/// <summary>
/// Helper class to streamline Mesap database manipulation scripts.
/// Use a "Private.resx" file to configure where to connect and
/// then run <see cref="ConnectAndExecute(Action{Database}[], string, bool)">.
/// </summary>
public class ScriptHelper
{
    private static readonly string DatabaseId = Private.DATABASE;

    /// <summary>
    /// Connect to Mesap database as specified in the private resource file using the login info
    /// presented there. Once the connection is established, run the callback actions provided sequentially.
    /// On error, fail gracefully. 
    /// </summary>
    /// <param name="actions">Callbacks to run upon successful connection.</param>
    /// <param name="appName">Application name to use when connecting to the Mesap Message Server.</param>
    /// <param name="runChecks">Check Mesap system and project database for missing features.</param>
    public static void ConnectAndExecute(Action<Database>[] actions, string appName = "Mesap Script", bool runChecks = false)
    {
        var app = new Application(new ApplicationContextBuilderOverwriteMessageServer(appName, Private.MESSAGE_SERVER));
        if (app.TryLogon(Private.USERNAME, Private.PASSWORD, out UserContext user))
        {
            try
            {
                if (app.TryOpenDatabase(DatabaseId, user, out DatabaseUserContext databaseUserContext))
                {
                    Console.WriteLine($"Database {databaseUserContext} opened.");

                    Database database = new(databaseUserContext);
                    if (runChecks)
                        RunChecks(database);

                    foreach (Action<Database> action in actions)
                        action.Invoke(database);
                }
                else
                    Console.WriteLine($"Failed to open Database {DatabaseId}. Error: {databaseUserContext.Validness.ExtendedErrorMessage}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex}");
            }
            finally
            {
                app.Logoff(user);
                app.Dispose();

                Console.WriteLine("Logoff complete, done.");
            }
        }
        else
            Console.WriteLine($"Aborted due to login failure: {user.GetLoginResultTranslation()}");
    }

    private static void RunChecks(Database database)
    {
        // Check the system database version
        var systemDatabase = new SystemDatabase(database.DatabaseUserContext.UserContext);
        if (!systemDatabase.CheckDatabaseCompatibility(out List<string> missingProgrammabilities))
        {
            Console.WriteLine("The system database is not up-to-date. Some framework features may not work correctly!");
            foreach (string missingProgrammability in missingProgrammabilities)
            {
                Console.WriteLine($"Missing: {missingProgrammability}");
            }
            Console.WriteLine("Consider updating your system database.");
        }
        else
        {
            Console.WriteLine("The system database is up-to-date.");
        }

        // Check the database version
        if (!database.CheckDatabaseCompatibility(out missingProgrammabilities))
        {
            Console.WriteLine($"The database {DatabaseId} is not up-to-date. Some framework features may not work correctly!");
            foreach (string missingProgrammability in missingProgrammabilities)
            {
                Console.WriteLine($"Missing: {missingProgrammability}");
            }
            Console.WriteLine("Consider updating your database.");
        }
        else
        {
            Console.WriteLine("The database is up-to-date.");
        }
    }
}
