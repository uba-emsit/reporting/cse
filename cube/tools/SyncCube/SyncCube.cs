﻿using CsvHelper;
using Mesap.Framework;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using System.Globalization;
using UBA.Mesap;

namespace UBA.CubeManager;

internal class SyncCube
{
    private const int MaxDescriptorIdLength = 64;
    private const int MaxDescriptorNameLength = 100;
    private const string DescriptorCsvFilePathPrefix = "../../../../../descriptors/";

    private static readonly List<CsvDescriptor> regions = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}regions.csv");
    private static readonly List<CsvDescriptor> categories = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}categories.csv", true);
    private static readonly List<CsvDescriptor> activities = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}activities.csv");
    private static readonly List<CsvDescriptor> types = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}types.csv");
    private static readonly List<CsvDescriptor> substances = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}pollutants.csv");
    private static readonly List<CsvDescriptor> fuels = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}fuels.csv");
    private static readonly List<CsvDescriptor> power_heat = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}power_heat.csv");
    private static readonly List<CsvDescriptor> firing_tech = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}firing_tech.csv");
    private static readonly List<CsvDescriptor> eb_flag = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}energy_balance_flag.csv");
    private static readonly List<CsvDescriptor> eb_row = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}energy_balance_row.csv");
    private static readonly List<CsvDescriptor> permit = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}permitting.csv");
    private static readonly List<CsvDescriptor> trinity = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}traffic_trinity.csv");
    private static readonly List<CsvDescriptor> vehicles = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}vehicles.csv");
    private static readonly List<CsvDescriptor> euro_norms = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}euro_norm.csv");
    private static readonly List<CsvDescriptor> products = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}product.csv");
    private static readonly List<CsvDescriptor> species = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}species.csv");
    private static readonly List<CsvDescriptor> manure = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}manure_management.csv");
    private static readonly List<CsvDescriptor> luc_from = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}land_use_categories_from.csv");
    private static readonly List<CsvDescriptor> luc_to = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}land_use_categories_to.csv");
    private static readonly List<CsvDescriptor> pools = ReadDescriptorMapping($"{DescriptorCsvFilePathPrefix}pools.csv");
    private static readonly Dictionary<string, List<CsvDescriptor>> dimensions = new()
    {
        {"REGION", regions},
        {"CATEGORY", categories},
        {"ACTIVITY", activities},
        {"TYPE", types},
        {"SUBSTANCE", substances},
        {"FUEL", fuels},
        {"POWER_HEAT_WORK", power_heat},
        {"FIRING_TECH", firing_tech},
        {"EB_FLAG", eb_flag},
        {"EB_ROW", eb_row},
        {"PERMIT", permit},
        {"TRANSPORT_TRINITY", trinity},
        {"VEHICLE_TYPE", vehicles},
        {"EURO_NORM", euro_norms},
        {"PRODUCT", products},
        {"SPECIES", species},
        {"MANURE_MANAGEMENT", manure},
        {"LUC_FROM", luc_from},
        {"LUC_TO", luc_to},
        {"POOL", pools}
    };

    private static bool UpdateWithoutConfirmation = false;

    static void Main(string[] args)
    {
        ScriptHelper.ConnectAndExecute([StartDimensionChecks, StartDescriptorChecks], appName: "ZSE Cube Manager", runChecks: false);
    }

    private static void StartDimensionChecks(Database database)
    {
        // Nothing fancy here, simply make sure csv and database match
        var mesapDimensions = database.Dimensions.ToList();
        var csvDimensions = ReadDimensionMapping("../../../../../dimensions.csv");

        foreach (Dimension mesapDimension in mesapDimensions)
        {
            if (!csvDimensions.Any(dim => dim.DimNr == mesapDimension.PrimaryKey))
                Console.WriteLine($"Found dimension missing in csv file {mesapDimension}");
        }

        foreach (CsvDimension csvDimension in csvDimensions)
        {
            var mesapDimension = database.Dimensions[csvDimension.DimNr];
            if (!mesapDimension.Id.ToString().Equals(csvDimension.ID) ||
            !mesapDimension.Name.ToString().Equals(csvDimension.label_de) ||
            !mesapDimension.Description.ToString().Equals(csvDimension.label_en))
            {
                Console.WriteLine($"\nWARNING: Found dimension mismatch with dimension {csvDimension}");
                Console.WriteLine($"Found dimension (DimNr: {mesapDimension.PrimaryKey}) to be out of sync:");
                Console.WriteLine($"CSV-FILE ID         : '{csvDimension.ID}'");
                Console.WriteLine($"Database ID         : '{mesapDimension.Id}'");
                Console.WriteLine($"CSV-FILE label_de   : '{csvDimension.label_de}'");
                Console.WriteLine($"Database Name       : '{mesapDimension.Name}'");
                Console.WriteLine($"CSV-FILE label_en   : '{csvDimension.label_en}'");
                Console.WriteLine($"Database Description: '{mesapDimension.Description}'");
            }
        }
    }

    private static void StartDescriptorChecks(Database database)
    {
        Console.WriteLine("Hit enter to start descriptor checks, use any other key to abort!");
        if (Console.ReadKey().Key == ConsoleKey.Enter)
        {
            foreach (var (dimId, csvDescriptors) in dimensions)
            {
                var mesapDescriptors = database.GetDescriptors(database.Dimensions[dimId]);

                // Step 1: Check CSV files against database contents (missing descriptors only)
                var mesapDescriptorKeys = new HashSet<int>(mesapDescriptors.Select(d => d.PrimaryKey));

                foreach (CsvDescriptor csvDescriptor in csvDescriptors)
                {
                    // Using a hashset here (instead of mesapDescriptors.Any()) for speed
                    if (!mesapDescriptorKeys.Contains(csvDescriptor.ObjNr))
                    {
                        Console.WriteLine($"WARNING: Found CSV entry missing in database: {csvDescriptor}");
                    }
                }

                // Step 2: Check database contents against CSV files
                foreach (Descriptor descriptor in mesapDescriptors)
                {
                    if (csvDescriptors.FirstOrDefault(item => item.ObjNr == descriptor.PrimaryKey) is { } csvDescriptor)
                    {
                        CheckDescriptor(database, dimId, descriptor, csvDescriptor, dimId.Equals("CATEGORY"));
                    }
                    else
                    {
                        Console.WriteLine($"WARNING: Found descriptor (ObjNr: {descriptor.PrimaryKey}) missing in csv file {descriptor.Id}/{dimId}");
                    }
                }

                Console.WriteLine($"Done checking dimension \"{dimId}\"");
            }
        }
    }

    private static void CheckDescriptor(Database database, string dimension, Descriptor descriptor, CsvDescriptor csvDescriptor, bool invertLang)
    {
        if (!descriptor.Id.ToString().Equals(csvDescriptor.ID) ||
            !descriptor.Name.ToString().Equals(invertLang ? csvDescriptor.label_en : csvDescriptor.label_de) ||
            !descriptor.Description.ToString().Equals(invertLang ? csvDescriptor.label_de : csvDescriptor.label_en))
        {
            Console.WriteLine($"Found descriptor (ObjNr: {descriptor.PrimaryKey}, dimension {dimension}) to be out of sync:");
            Console.WriteLine($"CSV-FILE ID         : '{csvDescriptor.ID}'");
            Console.WriteLine($"Database ID         : '{descriptor.Id}'");
            Console.WriteLine($"CSV-FILE label_de   : '{(invertLang ? csvDescriptor.label_en : csvDescriptor.label_de)}'");
            Console.WriteLine($"Database Name       : '{descriptor.Name}'");
            Console.WriteLine($"CSV-FILE label_en   : '{(invertLang ? csvDescriptor.label_de : csvDescriptor.label_en)}'");
            Console.WriteLine($"Database Description: '{descriptor.Description}'");

            if (UpdateWithoutConfirmation)
                UpdateDescriptor(database, descriptor, csvDescriptor, invertLang);
            else
            {
                Console.WriteLine("Hit <ENTER> to update the descriptor in the database, <ESC> to finish the importer, <A> to update this and all following descriptors or use any other key to skip!");

                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Escape:
                        throw new Exception("Get me out of here!");
                    case ConsoleKey.Enter:
                        UpdateDescriptor(database, descriptor, csvDescriptor, invertLang);
                        break;
                    case ConsoleKey.A:
                        UpdateWithoutConfirmation = true;
                        UpdateDescriptor(database, descriptor, csvDescriptor, invertLang);
                        break;
                }
            }
        }
    }

    private static void UpdateDescriptor(Database database, Descriptor descriptor, CsvDescriptor csvDescriptor, bool invertLang)
    {
        descriptor.Id = csvDescriptor.ID;
        descriptor.Name = invertLang ? csvDescriptor.label_en : csvDescriptor.label_de;
        descriptor.Description = invertLang ? csvDescriptor.label_de : csvDescriptor.label_en;

        if (new DescriptorWriter(database.DatabaseUserContext).Write(descriptor).UpdatedEntities.Count == 1)
        {
            Console.WriteLine($"Descriptor (ObjNr: {descriptor.PrimaryKey}, dimension {descriptor.Dimension.Name}) is now:");
            Console.WriteLine($"Database ID         : '{descriptor.Id}'");
            Console.WriteLine($"Database Name       : '{descriptor.Name}'");
            Console.WriteLine($"Database Description: '{descriptor.Description}'");
        }
        else
            Console.WriteLine($"FAILED to update descriptor (ObjNr: {descriptor.PrimaryKey})!");
    }

    private static List<CsvDimension> ReadDimensionMapping(string fromFile)
    {
        using (var reader = new StreamReader(fromFile))
        using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
        {
            return csvReader.GetRecords<CsvDimension>().ToList();
        }
    }

    internal class CsvDimension
    {
        public int DimNr { get; set; }
        public string? ID { get; set; }
        public string? label_de { get; set; }
        public string? label_en { get; set; }

        public override string? ToString()
        {
            return $"{DimNr}: {ID}, {label_en}";
        }
    }

    private static List<CsvDescriptor> ReadDescriptorMapping(string fromFile, bool invertLang = false)
    {
        using (var reader = new StreamReader(fromFile))
        using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
        {
            List<CsvDescriptor> result = [];

            foreach (CsvDescriptor descriptor in csvReader.GetRecords<CsvDescriptor>())
            {
                if (descriptor.ID == null || descriptor.ID.Length > MaxDescriptorIdLength)
                    Console.WriteLine($"Bad csv entry skipped: ID is \"{descriptor.ID}\"");
                else if (!invertLang && (descriptor.label_de == null || descriptor.label_de.Length > MaxDescriptorNameLength))
                    Console.WriteLine($"Bad csv entry skipped: Label (German) is \"{descriptor.label_de}\"");
                else if (invertLang && (descriptor.label_en == null || descriptor.label_en.Length > MaxDescriptorNameLength))
                    Console.WriteLine($"Bad csv entry skipped: Label (English) is \"{descriptor.label_en}\"");
                else if (descriptor.ID != null && descriptor.label_de != null && descriptor.label_en != null)
                    result.Add(descriptor);
                else
                    Console.WriteLine($"Bad csv entry skipped: ObjNr is \"{descriptor.ObjNr}\"");
            }

            return result;
        }
    }

    internal class CsvDescriptor
    {
        public int ObjNr { get; set; }
        public string? ID { get; set; }
        public string? label_de { get; set; }
        public string? label_en { get; set; }

        public override string? ToString()
        {
            return $"{ObjNr}: {ID}, {label_en}";
        }
    }
}
