SELECT Descriptor.ObjNr,
       Dimension.Id AS Dimension,
       Descriptor.Id, Descriptor.Name,
       COALESCE(Descriptor.CountSeries, 0) AS "Use count in time series",
       COALESCE(Helper.CountTrees, 0) AS "Use count in trees"
FROM (
  SELECT TreeObject.ObjNr, TreeObject.DimNr, TreeObject.Id, TreeObject.Name, TimeSeriesKey.CountSeries
  FROM TreeObject
  LEFT JOIN (
    SELECT ObjNr, COUNT(*) AS CountSeries
    FROM TimeSeriesKey
    GROUP BY ObjNr) TimeSeriesKey
  ON TreeObject.ObjNr = TimeSeriesKey.ObjNr) AS Descriptor
INNER JOIN (
  SELECT TreeObject.ObjNr, TreeStructure.CountTrees
  FROM TreeObject
  LEFT JOIN (
    SELECT ReferenceNr, COUNT(*) AS CountTrees
    FROM TreeStructure
    GROUP BY ReferenceNr) TreeStructure
  ON TreeObject.ObjNr = TreeStructure.ReferenceNr) AS Helper
ON Descriptor.ObjNr = Helper.ObjNr
INNER JOIN Dimension ON Descriptor.DimNr = Dimension.DimNr
ORDER BY Descriptor.CountSeries DESC
