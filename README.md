# The Central System of Emissions (German: "Zentrales System Emissionen", ZSE)

Since 1998, the Federal Environment Agency has maintained and managed an IT tool for inventory preparation: The Central System of Emissions (CSE), an integrated national database. The CSE implements the diverse requirements pertaining to emissions calculation and reporting, and it automates key steps in such work. It supports the processes of inventory planning and reporting (for example, by carrying out emissions calculations and recalculations, and relevant error analysis); inventory management (for example, by carrying out archiving and annual data evaluation); and quality management at the data level. The CSE makes it possible to fulfil the key requirements of transparency, consistency, completeness, comparability and accuracy at the data level.

Data documentation plays a central role in the CSE. The CSE stores such information as who is responsible for handling specific tasks; data sources and calculation procedures; and uncertainties in time-series values. The times at which changes are made, and the persons by whom they are made, are also recorded. The system has a history-management function that archives deleted items and can restore them as necessary. This makes it possible to trace back and reconstruct data, and it enables third parties to carry out independent reviews. The system also provides mechanisms that support quality assurance at the data level (e.g. components for detecting uncertainties and checking plausibility). Above all, transparency is accommodated by ensuring that data are recorded within the same structure in which they are provided, and that all processing and transformations into a reporting format take place first in the CSE itself, and thus remain open to examination. In addition, the CSE manages detailed technology-specific activity data and emission factors that can be processed, via calculation rules (calculation methods), into aggregated, category-specific values for the various reporting formats. In addition to aggregation and model formation for calculations, the CSE also supports scenario and forecast calculations and use of the reference approach.

## Repository structure

This repository documents the CSE's structure and also contains some of the contents. Note, however, that it does *not* contain the emission data itself at a detailed level, as some of those data are confidential.

The contents provided here can be used to recreate the CSE's structure and is synced with the federal agency's production database regularly. The resources are organized as follows:

1. **Cube:** The data cube structure of the database, listing all dimensions, descriptors and trees used. Includes dimension details such as classification and icons as well as German/English labels for all structural objects. Start here for an overview on the CSE's structure and contents.
2. **Time series:** While not offering the time series data itself, this folder has all the time series in the database listed, including their names, IDs, descriptors and other metadata.
3. **Scripts:** Copies of the calculation scripts used in the CSE database for special processing needs of raw data
4. **Import:** Contains scripts and result files to import other member state's emission data into the database of comparison with the German data.
5. **Export:** Information and files needed to facilitate data transfer between the CSE database and other tools/institutions.

Beyond these major resources, the repository also offers database maintenance tools and other details which are typically not of public interest.
