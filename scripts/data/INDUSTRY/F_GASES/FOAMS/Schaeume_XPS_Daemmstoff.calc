Input Anw_EF_1234ze: 2F2_EF_Bestand_1234ze_XPS (Emissionsfaktor HFC-1234ze von XPS-Dämmstoffen in der Anwendung)
Input Anw_EF_134a: ER-bank (HFC134a-Emissionsfaktor von XPS-Dämmstoffen in der Anwendung)
Input Prod_ANT_Inlandsverbrauch_1234ze: Ant_Anw_1234ze_Produktion (Anteil Inlandsverbrauch der deutschen XPS-Dämmstoffproduktion mit HFC-1234ze)
Input Prod_ANT_Inlandsverbrauch_134a: Non-Exp (Anteil Inlandsverbrauch der deutschen XPS-Dämmstoffproduktion mit HFC-134a)
Input Prod_AR_1234ze: 2F2_AR_Prod_1234ze_XPS (Inlandsverbrauch HFC-1234ze für XPS-Dämmstoffe)
Input Prod_AR_134a: Inlandsverbrauch 134a XPS (HFC134a-Inlandsverbrauch für XPS-Dämmstoffe)
Input Prod_AR_152a: Inlandsverbrauch 152a XPS (HFC152a-Inlandsverbrauch für XPS-Dämmstoffe)
Input Prod_EF_1234ze: 2F2_EF_Prod_1234ze_XPS (Fertigungsemissionsfaktor HFC-1234ze von XPS-Dämmstoffen)
Input Prod_EF_134a: Loss-134a (HFC134a-Fertigungsemissionsfaktor von XPS-Dämmstoffen)
Input Prod_EF_152a: Loss-152a (HFC152a-Fertigungsemissionsfaktor von XPS-Dämmstoffen)
Output Anw_AR_1234ze: 2F2_EB_Bestand_1234ze_XPS (Endbestand HFC-1234ze in XPS-Dämmstoffen)
Output Anw_AR_134a: Erg_Domest_bank (Endbestand HFC-134a in XPS-Dämmstoffen)
Output Anw_EM_1234ze: 2F2_EM_Bestand_1234ze_XPS (Bestandsemissionen HFC-1234ze aus XPS-Dämmstoffen)
Output Anw_EM_134a: XPS_Schaum_Anw_EM_134a (Bestandsemissionen HFC-134a von XPS-Dämmstoffen)
Output Anw_NZG_1234ze: 2F2_NZG_Bestand_1234ze_XPS (Neuzugang HFC-1234ze in XPS-Dämmstoffen)
Output Anw_NZG_134a: Erg_HFC_increase (Neuzugang HFC-134a in XPS-Dämmstoffen)
Output Prod_EM_1234ze: 2F2_EM_Prod_1234ze_XPS (Fertigungsemissionen HFC-1234ze von XPS-Dämmstoffen)
Output Prod_EM_134a: XPS_Schaum_Prod_EM_134a (Fertigungsemissionen HFC-134a von XPS-Dämmstoffen)
Output Prod_EM_152a: XPS_Schaum_Prod_EM_152a (Fertigungsemissionen HFC-152a von XPS-Dämmstoffen)

// Berechnungsverfahren für XPS-Dämmstoffe. 
// Eingesetzte Treibmittel: HFC-134a (seit 2001), HFC-152a (seit 2001) und HFC-1234ze (seit 2012). 
// Produktion und Anwendung ab 2001, Entsorgung ab 2051 (Lebensdauer 50 Jahre).
// In Deutschland wird mehr produziert als angewendet. Ein bestimmter Anteil wird exportiert. 
// HFC-152a emittiert vollständig bei der Produktion (open cells). Bei HFC-134a und HFC-1234ze emittiert nur ein Teil bei der Produktion, der Rest gast während der Nutzungsphase und bei der Entsorgung aus (closed cells). 
// Berechnung ab 1939 starten. 


//1. Produktion ab 2001
//1.1 Aktivitätsrate
[1990-]
// Prod_AR_134a   = Input
// Prod_AR_152a   = Input
// Prod_AR_1234ze = Input

//1.2 Emissionen
Prod_EM_134a      = Prod_AR_134a   * Prod_EF_134a;
Prod_EM_152a      = Prod_AR_152a   * Prod_EF_152a;			//Martens: Prod_EF_152a = 100%
Prod_EM_1234ze    = Prod_AR_1234ze * Prod_EF_1234ze;

//2. Anwendung ab 2001 (nur closed cells)
//2.1 Aktivitätsrate
[1939-]
Anw_NZG_134a      = (Prod_AR_134a   * (1 - Prod_EF_134a))   * Prod_ANT_Inlandsverbrauch_134a;
Anw_NZG_1234ze    = (Prod_AR_1234ze * (1 - Prod_EF_1234ze)) * Prod_ANT_Inlandsverbrauch_1234ze; 

[1989-]
Anw_AR_134a       = AggTime(Sum; Anw_NZG_134a; t-50; t);
Anw_AR_1234ze     = AggTime(Sum; Anw_NZG_1234ze; t-50; t);

//2.2 Emissionen
[1990-]
Anw_EM_134a       = Anw_AR_134a   * Anw_EF_134a;
Anw_EM_1234ze     = Anw_AR_1234ze * Anw_EF_1234ze;


//3. Entsorgung ab 2051 (nur HFC-134a)
// Entsorgung gibt es bisher nicht. Bei einer Lebensdauer von 50 Jahren (Quelle: IPCC Good Practice Guidance 2000: Product life in years 50a) müssen Entsorgungsemissionen voraussichtlich erst ab dem Jahr 2051 berichtet werden.

